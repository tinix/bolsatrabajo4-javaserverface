package beans.model;

import java.util.Date;
import javax.annotation.ManagedBean;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author tinix
 */
@ManagedBean
@Named(value = "candidato")
@RequestScoped
public class Candidato {

   
    public Candidato() {
    }
    
    private String nombre = "";

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

//    public Object getNombre(String nombre) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }  
    
    private String apellido = "Introduce Apellido";

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
        private int sueldoDeseado;

    public int getSueldoDeseado() {
        return sueldoDeseado;
    }

    public void setSueldoDeseado(int sueldoDeseado) {
        //System.out.println("Modificacion del Modelo . Valor sueldo :" + sueldoDeseado);
        this.sueldoDeseado = sueldoDeseado;
    }
    
    private Date fechaNacimiento;

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    
}